#!/bin/bash
xhost +local:docker

docker run --rm -it \
	--env="DISPLAY" \
	--env="QT_X11_NO_MITSHM=1" \
	--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
	--volume="$HOME/studio/master/reinforcement:/root/studio/reinforcement" \
	--privileged \
	benthepleb/summerstudio2020:nightly \
	bash
