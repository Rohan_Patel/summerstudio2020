# Contains the model architecture that we are appending
from tensorflow.keras.layers import Dense,GlobalAveragePooling2D, Conv2D, MaxPooling2D, Dropout, Flatten
from tensorflow.keras.models import Model, Sequential
from auxilaries import *

def append_model(base_model):
	x = base_model.output
	x = GlobalAveragePooling2D()(x)
	x = Dense(1024, activation='relu')(x)
	x = Dense(1024, activation='relu')(x)
	x = Dense(1024, activation='relu')(x)
	preds = Dense(NUM_CLASSES, activation='softmax')(x)
	model = Model(inputs = base_model.input, outputs = preds)
	return model

def freeze_layers(model, layer_index):
	for layer in model.layers[:layer_index]:
		layer.trainable = False
	for layer in model.layers[layer_index:]:
		layer.trainable = True

def basic_model(input_shape):
	model = Sequential()
	model.add(Conv2D(32, (3, 3), activation='relu', input_shape=input_shape))
	model.add(Conv2D(32, (3, 3), activation='relu'))
	model.add(MaxPooling2D(pool_size=(2, 2)))
	model.add(Dropout(0.25))
	model.add(Conv2D(64, (3, 3), activation='relu'))
	model.add(Conv2D(64, (3, 3), activation='relu', name='last_conv_layer'))
	model.add(MaxPooling2D(pool_size=(2, 2)))
	model.add(Dropout(0.25))
	model.add(Flatten())
	model.add(Dense(256, activation='relu'))
	model.add(Dropout(0.5))
	model.add(Dense(2, activation='softmax'))
	return model