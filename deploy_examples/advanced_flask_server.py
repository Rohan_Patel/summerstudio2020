import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.mobilenet import preprocess_input
from tensorflow.keras.models import load_model
from auxilaries import *


# Need this section to stop local GPU from crashing due to memory issues
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

config = ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.2
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)

if transfer == True:
    model = load_model(fine_tuned_weight_path)
else:
    model = load_model(normal_weight_path)

# Flask server specific imports
from flask import Flask, redirect, url_for, request, render_template
from werkzeug.utils import secure_filename

# Define a flask app and configure an upload directory
app = Flask('ImageBot')
app.config['UPLOAD_FOLDER'] = 'static/uploads/'

# A function to run our predictions
def model_predict(img_path, model):
    img = image.load_img(img_path, target_size=TARGET_SIZE)
    img_array = image.img_to_array(img)
    img_array_expanded_dims = np.expand_dims(img_array, axis=0)
    processed_img = preprocess_input(img_array_expanded_dims)
    prediction = model.predict(processed_img)
    return prediction

# Generate the HTML template that we have constructed
@app.route('/')
def hello():
    return render_template('upload.html')

# Generate the prediction from the uploads folder
@app.route('/', methods=['GET', 'POST'])
def upload_file():
     if request.method == 'POST':
		# Get the file from post request
        f = request.files['file']

		# Save the file to ./uploads
        file_path = os.path.join(
			app.config['UPLOAD_FOLDER'], secure_filename(f.filename))
        f.save(file_path)
        print(file_path)

        abs_file_path = os.path.abspath(file_path)

        # Make prediction
        prediction = model_predict(file_path, model)

        # Decode the predictions to a string format
        string1 = "HotDog Certainty: "+ str(prediction[0,0]*100)
        string2 = "Not HotDog Certainty: " + str(prediction[0,1]*100)
        prediction_index = prediction.argmax()
        prediction = np.max(prediction)

        string3 = "The predicted item is a " + classes[int(prediction_index)] + " with " + str(prediction*100) +  " certainity"

        print("The predicted item is a " + classes[int(prediction_index)] + " with %.2f%% certainity \n\n" %(prediction*100))

        return render_template('result.html', result1 = string1, result2 = string2, result3 = string3, url=file_path)
     return render_template('upload.html')

if __name__ == '__main__':
	app.run(host='0.0.0.0', debug = True)
