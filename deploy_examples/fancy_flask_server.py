import sys
import os
import cv2, os
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.mobilenet import preprocess_input
from tensorflow.keras.models import load_model
from auxilaries import *
from flask import Flask, redirect, url_for, request, render_template, session
from werkzeug.utils import secure_filename

# Extra scripts
from camera import *

# Need this section to stop local GPU from crashing due to memory issues
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

config = ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.2
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)

if transfer == True:
    model = load_model(fine_tuned_weight_path)
else:
    model = load_model(normal_weight_path)

# Create a camera object
cap = create_camera()
# Define a flask app and configure an upload directory
app = Flask(import_name = 'fancy flask', static_folder = "fancy_static", template_folder ="fancy_template")
app.config['UPLOAD_FOLDER'] = 'fancy_static/uploads/'

# global variables to store image and path
resultTest = str()
file_path = str()
cam_used = False

# A function to run our predictions
def model_predict(img_path, model):
    img = image.load_img(img_path, target_size=TARGET_SIZE)
    img_array = image.img_to_array(img)
    img_array_expanded_dims = np.expand_dims(img_array, axis=0)
    processed_img = preprocess_input(img_array_expanded_dims)
    prediction = model.predict(processed_img)
    return prediction

# Generate the HTML template that we have constructed
@app.route('/', methods=['GET'])
def home():
	return render_template('upload.html', result = resultTest, url = file_path)

# Generate the prediction from the uploads folder
@app.route('/', methods=['POST'])
def upload_file():
	if request.method == 'POST':
		if request.form['submit_button'] == 'Submit':
			# debugging comment
			print("Cam Used to submit photo is :", cam_used)

			# handles uplaod and camera inputs options
			# currently only uplaod file input option works, camera code is buggy
			if cam_used == False:
				f = request.files['file']
				# Save the file to ./uploads
				file_path = os.path.join(
				app.config['UPLOAD_FOLDER'], secure_filename(f.filename))
				f.save(file_path)
				print(file_path)
				abs_file_path = os.path.abspath(file_path)
				# Make prediction
				prediction = model_predict(file_path, model)
			else:
				resultTest = session.get('result')
				file_path = session.get('filepath')

			prediction_index = prediction.argmax()
			resultTest = classes[int(prediction_index)]

			return render_template('result.html', result = resultTest, url = file_path)

# //rendering the HTML page which has the button
@app.route('/json')
def json():
	return render_template('upload.html')

# //background process happening without any refreshing
@app.route('/background_process_test', methods=['POST', 'GET'])
def background_process_test():

	# code that handles camera option in background of web page
	cam_used = True
	print("Cam Used after camera photo is :", cam_used)
	cameraTake(cap)
	resultTest = model_predict('fancy_static/uploads/0.jpg', model)
	print(resultTest)
	file_path = "fancy_static/uploads/0.jpg"
	session['result'] == resultTest
	session['filepath'] == file_path

	# returns the original html as this is a background operation
	return render_template('upload.html', results=resultTest, url=file_path)

if __name__ == '__main__':
	resultTest = str()
	file_path = str()
	cam_used = False
	app.run(host='0.0.0.0', debug = True)
