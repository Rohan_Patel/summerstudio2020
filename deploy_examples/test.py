import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.mobilenet import preprocess_input
from tensorflow.keras.models import load_model
from auxilaries import *

if transfer == True:    
    model = load_model(fine_tuned_weight_path)
else:
    model = load_model(normal_weight_path)

for num in range(len(list_of_images)):
    # Preprocess images that were done in the training phase 
    img = image.load_img(test_directory + list_of_images[num], target_size=TARGET_SIZE)
    img_array = image.img_to_array(img)
    img_array_expanded_dims = np.expand_dims(img_array, axis=0)
    processed_img = preprocess_input(img_array_expanded_dims)
    prediction = model.predict(processed_img)

    print("HotDog Certainty:  %.2f%%" % (prediction[0,0]*100))
    print("Not HotDog Certainty:  %.2f%%" % (prediction[0,1]*100))
    prediction_index = prediction.argmax()
    prediction = np.max(prediction)

    print("The predicted item is a " + classes[int(prediction_index)] + " with %.2f%% certainity \n\n" %(prediction*100))
    plt.figure()
    plt.imshow(img)
    plt.show()
