#!/bin/bash

fileNameIndex=0   # set to index of file name
categoryIndex=1   # set to index of category

IFS=",""$IFS"     # add comma to break lines at commas

while read -a tokens;    # read a line and break it into tokens separated by commas
do
    file=${tokens[$fileNmeIndex]}       # get the file name
    category=${tokens[$categoryIndex]}  # get the category
    if [ ! -d $category ]; then         # check if the category directory exists
        mkdir $category;                # make the category directory
    fi
    mv $file $category                  # move the file into the category directory
done
